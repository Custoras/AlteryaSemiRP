package fr.shyndard.alteryasemirp;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryasemirp.function.CustomEntityManager;
import net.md_5.bungee.api.ChatColor;

public class CmdAdmin implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(pi.getRank().getPower() > 1) return false;
		if(args.length > 0) {
			if(args[0].equals("monster")) {
				if(args.length == 2) {
					CustomEntityManager.spawnMonster(player, Integer.parseInt(args[1]));
				}
				return false;
			}
			if(args[0].equals("title")) {
				if(args.length >= 2) {
					String name = args[1];
					for(int i = 2; i < args.length; i++) name+=(" " + args[i]);
					CustomEntityManager.spawnTitle(player, ChatColor.translateAlternateColorCodes('&', name));
				}
				return false;
			}
			if(args[0].equals("delas")) {
				CustomEntityManager.despawn(player);
				return false;
			}
		}
		return false;
	}
}
