package fr.shyndard.alteryasemirp;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryasemirp.function.TutorialManager;
import net.md_5.bungee.api.ChatColor;

public class CmdTutoriel2 implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(pi.getRank().getPower() > 1) {
			sender.sendMessage(ChatColor.RED + "Vous n'y avez plus acc�s.");
			return true;
		}
		if(args.length == 1 && TutorialManager.exist(player)) TutorialManager.get(player).nextStep();
		else TutorialManager.add(player);
		return true;
	}
}