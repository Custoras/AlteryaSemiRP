package fr.shyndard.alteryasemirp;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import fr.shyndard.alteryasemirp.event.MapRender;
import fr.shyndard.alteryasemirp.event.PlayerConnection;
import fr.shyndard.alteryasemirp.event.PlayerDeath;
import fr.shyndard.alteryasemirp.event.PlayerInteractMenu;
import fr.shyndard.alteryasemirp.event.PlayerInteractNPC;
import fr.shyndard.alteryasemirp.event.PlayerMove;
import fr.shyndard.alteryasemirp.function.BallooningManagement;
import fr.shyndard.alteryasemirp.function.BardManager;
import fr.shyndard.alteryasemirp.method.NewTutoriel;
import fr.shyndard.alteryasemirp.method.Tutoriel;

public class Main extends JavaPlugin {

	static Main plugin;
	static Map<Player, String> map_url = new HashMap<>();
	public static boolean enable;
	
	public void onEnable() {
		plugin = this;
		getServer().getPluginManager().registerEvents(new MapRender(), this);
		getServer().getPluginManager().registerEvents(new PlayerConnection(), this);
		getServer().getPluginManager().registerEvents(new PlayerDeath(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractMenu(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractNPC(), this);
		getServer().getPluginManager().registerEvents(new PlayerMove(), this);
		getCommand("admin").setExecutor(new CmdAdmin());
		getCommand("map").setExecutor(new CmdMap());
		getCommand("tutoriel").setExecutor(new CmdTutoriel());
		getCommand("tutoriel2").setExecutor(new CmdTutoriel2());
		getCommand("bard").setExecutor(new CmdBard());
		getCommand("ballooning").setExecutor(new CmdBallooning());
		Tutoriel.init();
		NewTutoriel.init();
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  BallooningManagement.initMenu();
	          }
		}, 5*20L);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  BardManager.init();
	          }
		}, 6*20L);
	}
	
	public void onDisable() {
		BallooningManagement.removeAll();
	}

	public static Plugin getPlugin() {
		return plugin;
	}

	public static Map<Player, String> getMapUrl() {
		return map_url;
	}

	public static void addUrl(Player player, String url) {
		map_url.put(player, url);
	}
}
