package fr.shyndard.alteryasemirp;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryasemirp.function.BallooningManagement;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class CmdBallooning implements CommandExecutor {

	String prefix = ChatColor.GRAY + "[Mongolgi�re] ";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(prefix + "La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("ballooning.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if(pi.hasPermission("ballooning.admin") && args.length > 0) {
			if(args[0].equalsIgnoreCase("reload")) {
				BallooningManagement.initMenu();
				player.sendMessage(prefix + ChatColor.GREEN + "Menu actualis�.");
				return false;
			} else if(args[0].equalsIgnoreCase("set")) {
				if(args.length == 4) {
					Integer npc_id = null;
					try {
						npc_id = Integer.parseInt(args[2]);
					} catch(Exception ex) {
						sender.sendMessage(prefix + ChatColor.RED + "Veuillez saisir un identifiant (format d'ENTIER)");
						return false;
					}
					Integer slot = null;
					try {
						slot = Integer.parseInt(args[3]);
					} catch(Exception ex) {
						sender.sendMessage(prefix + ChatColor.RED + "Veuillez saisir un slot menu (format d'ENTIER)");
						return false;
					}
					BallooningManagement.set(npc_id, args[1], player.getLocation(), slot);
					player.sendMessage(prefix + ChatColor.GREEN + "Mongolfi�re actualis�e.");
				} else {
					sender.sendMessage(prefix + ChatColor.GRAY + "Utilisation : /ballooning set <name> <npc_id> <menu_slot>");
				}
				return false;
			} else if(args[0].equalsIgnoreCase("delete")) {
				if(args.length == 2) {
					if(BallooningManagement.deleteBallooning(args[1])) {
						sender.sendMessage(prefix + ChatColor.GREEN + "Mongolfi�re d�truite.");
					} else {
						sender.sendMessage(prefix + ChatColor.RED + "Mongolfi�re introuvable.");
					}
				} else {
					sender.sendMessage(prefix + ChatColor.GRAY + "Utilisation : /ballooning delete <name>");
				}
			}
		}
		Integer npc_id;
		try {
			npc_id = Integer.parseInt(args[1]);
		} catch(Exception ex) {
			sender.sendMessage(prefix + ChatColor.RED + "Format commande incorrect.");
			return false;
		}
		NPC npc = CitizensAPI.getNPCRegistry().getById(npc_id);
		if(npc == null) {
			sender.sendMessage(prefix + ChatColor.RED + "PNJ introuvable.");
			return false;
		}
		if(player.getLocation().distance(npc.getStoredLocation()) > 5) {
			sender.sendMessage(prefix + ChatColor.RED + "Vous �tes trop �loign� pour parler.");
			return false;
		}
		if(!BallooningManagement.existPilot(npc_id)) {
			NPCAPI.talkTo(npc, player, "D�sol�, je ne suis pas un pilote.");
			return false;
		}
		if(args[0].equalsIgnoreCase("openmenu")) {
			BallooningManagement.openMenu(player);
			return false;
		}
		if(args[0].equalsIgnoreCase("deny")) {
			NPCAPI.talkTo(npc, player, "Je ne bouge pas. Viens me voir quand tu veux.");
			return false;
		}
		if(pi.hasPermission("ballooning.admin")) { sendHelp(sender); }
		else sender.sendMessage(ChatColor.RED + "Format de la commande incorrect.");
		return false;
	}
	
	void sendHelp(CommandSender sender) {
		sender.sendMessage(ChatColor.GRAY + "Commandes d�placement en mongolfi�re");
		sender.sendMessage(ChatColor.GRAY + "/ballooning set <name> <npc_id> <menu_slot>");
		sender.sendMessage(ChatColor.GRAY + "/ballooning delete <name> <npc_id> <menu_slot>");
	}

}
