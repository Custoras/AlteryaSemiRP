package fr.shyndard.alteryasemirp.method;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.EntityTypes;
import net.minecraft.server.v1_12_R1.World;

@SuppressWarnings("unchecked")
public class ArmorStandTitle extends EntityArmorStand {

    public UUID player_uuid;

    public ArmorStandTitle(World world, Player p) {
        super(world);
        player_uuid = p.getUniqueId();
    }

    @Override
    public void B_() {
        EntityPlayer p = ((CraftPlayer) Bukkit.getPlayer(player_uuid)).getHandle();	    
        setPosition(p.locX, p.locY+1.1, p.locZ);
    }

    public static ArmorStandTitle SPAWN(Player p, String name) {
        World w = ((CraftWorld) p.getWorld()).getHandle();
        ArmorStandTitle armorStand = new ArmorStandTitle(w, p);
        armorStand.setLocation(p.getLocation().getX(), p.getLocation().getY()+1.1, p.getLocation().getZ(), p.getLocation().getYaw(), p.getLocation().getPitch());
        ArmorStand as = (ArmorStand)armorStand.getBukkitEntity();
        setArmorStand(as, name);
        w.addEntity(armorStand, SpawnReason.CUSTOM);
        return armorStand;
    }

	public static void registerEntity() {
        try {
            Class<EntityTypes> entityTypeClass = EntityTypes.class;
            Field c = entityTypeClass.getDeclaredField("c");
            c.setAccessible(true);
            
			@SuppressWarnings("rawtypes")
			HashMap<String, Class<?>> c_map = (HashMap) c.get(null);
            c_map.put("customArmorStand", ArmorStandTitle.class);

            Field d = entityTypeClass.getDeclaredField("d");
            d.setAccessible(true);
            HashMap<Class<?>, String> d_map = (HashMap) d.get(null);
            d_map.put(ArmorStandTitle.class, "customArmorStand");

            Field e = entityTypeClass.getDeclaredField("e");
            e.setAccessible(true);
            HashMap<Integer, Class<?>> e_map = (HashMap) e.get(null);
            e_map.put(Integer.valueOf(63), ArmorStandTitle.class);

            Field f = entityTypeClass.getDeclaredField("f");
            f.setAccessible(true);
            HashMap<Class<?>, Integer> f_map = (HashMap) f.get(null);
            f_map.put(ArmorStandTitle.class, Integer.valueOf(63));

            Field g = entityTypeClass.getDeclaredField("g");
            g.setAccessible(true);
            HashMap<String, Integer> g_map = (HashMap) g.get(null);
            g_map.put("customArmorStand", Integer.valueOf(63));

        } catch (Exception exc) {
            Field d;
            int d_map;
            Method[] e;
            Class[] paramTypes = { Class.class, String.class, Integer.TYPE };
            try {
                Method method = EntityTypes.class.getDeclaredMethod(
                        "addMapping", paramTypes);
                method.setAccessible(true);
            } catch (Exception ex) {
                exc.addSuppressed(ex);
                try {
                    d_map = (e = EntityTypes.class.getDeclaredMethods()).length;
                    for (int d1 = 0; d1 < d_map; d1++) {
                        Method method = e[d1];
                        if (Arrays.equals(paramTypes,
                                method.getParameterTypes())) {
                            method.invoke(null, new Object[] {
                                    ArmorStandTitle.class,
                                    "customArmorStand", Integer.valueOf(63) });
                        }
                    }
                } catch (Exception exe) {
                    exc.addSuppressed(exe);
                }
                exc.printStackTrace();
            }
        }
    }
	
	private static void setArmorStand(ArmorStand as, String name) {
		as.setBasePlate(false);
		as.setSmall(true);
		as.setInvulnerable(true);
		as.setGravity(false);
		as.setArms(true);
		as.setVisible(false);
		as.setCollidable(false);
		as.setCanPickupItems(false);
		as.setNoDamageTicks(Integer.MAX_VALUE);
		as.setCustomName(name);
		as.setCustomNameVisible(true);
	}
}