package fr.shyndard.alteryasemirp.method;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;

public class Ballooning {

	private Integer pilot_id;
	private Integer menu_position;
	private Location location;
	private String name;
	private ArmorStand as;
	static String airplane_char = Character.toString('\u2708');
	
	public Ballooning(Integer pilot_id, Integer menu_position, Location location, String name) {
		this.pilot_id = pilot_id;
		this.menu_position = menu_position;
		this.location = location;
		this.name = name;
		NPC npc = CitizensAPI.getNPCRegistry().getById(pilot_id);
		if(npc == null) return;
		Chunk c = npc.getStoredLocation().getChunk();
		c.load();
		for(Entity e : npc.getStoredLocation().getWorld().getNearbyEntities(npc.getStoredLocation(), 2, 2, 2)) {
			if(e.getType() == EntityType.ARMOR_STAND && e.getCustomName() != null) {
				if(((ArmorStand)e).getCustomName().equals("" + ChatColor.GOLD + ChatColor.BOLD + airplane_char)) e.remove();
			}
		}
		as = (ArmorStand)npc.getStoredLocation().getWorld().spawnEntity(npc.getStoredLocation().clone().add(0,0.1,0), EntityType.ARMOR_STAND);
		as.setBasePlate(false);
		as.setGravity(false);
		as.setVisible(false);
		as.setCustomNameVisible(true);
		as.setCustomName("" + ChatColor.GOLD + ChatColor.BOLD + airplane_char);
	}
	
	public Integer getPilotId() { return pilot_id; }
	
	public Integer getMenuPosition() { return menu_position; }
	
	public Location getArrivalLocation() { return location; }
	
	public String getArrivalName() { return name; }
	
	public void remove() {
		if(as == null) return;
		as.getLocation().getChunk().load();
		as.remove();
	}
}
