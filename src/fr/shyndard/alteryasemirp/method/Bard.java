package fr.shyndard.alteryasemirp.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryasemirp.Main;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;

public class Bard {

	private NPC npc;
	private ArmorStand as;
	private Integer bard_id;
	private List<String> speech = new ArrayList<>();
	private static String music_char = Character.toString('\u266B');
	
	public Bard(Integer bard_id) {
		this.bard_id = bard_id;
		npc = CitizensAPI.getNPCRegistry().getById(bard_id);
		if(npc == null) return;
		Chunk c = npc.getStoredLocation().getChunk();
		c.load();
		for(Entity e : npc.getStoredLocation().getWorld().getNearbyEntities(npc.getStoredLocation(), 2, 2, 2)) {
			if(e.getType() == EntityType.ARMOR_STAND && e.getCustomName() != null) {
				if(((ArmorStand)e).getCustomName().equals("" + ChatColor.GREEN + ChatColor.BOLD + music_char)) e.remove();
			}
		}
		as = (ArmorStand)npc.getStoredLocation().getWorld().spawnEntity(npc.getStoredLocation().clone().add(0,0.1,0), EntityType.ARMOR_STAND);
		as.setBasePlate(false);
		as.setGravity(false);
		as.setVisible(false);
		as.setCustomNameVisible(true);
		as.setCustomName("" + ChatColor.GOLD + ChatColor.BOLD + music_char);
		loadSpeech();
	}
	
	private void loadSpeech() {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT speed_message FROM bard_speech WHERE bard_id = " + bard_id +  " ORDER BY speech_index ASC");
			while(result.next()) speech.add(result.getString("speed_message"));
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public void play() {
		speech(0);
	}
	
	private void speech(Integer index) {
		NPCAPI.talkRadius(npc, speech.get(index), 8);
		if(index+1 < speech.size()) {
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
		          public void run() { speech(index+1); }
			}, 20*20L);
		}
	}
	
	public Integer getBardId() { return bard_id; }
	
	public void remove() {
		if(as == null) return;
		as.getLocation().getChunk().load();
		as.remove();
	}
}
