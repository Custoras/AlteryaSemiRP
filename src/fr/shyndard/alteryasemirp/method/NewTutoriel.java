package fr.shyndard.alteryasemirp.method;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.Title;
import net.md_5.bungee.api.ChatColor;

public class NewTutoriel {

	private Player player;
	private Integer index = 0;
	
	private static Map<Integer, String> msg_title = new HashMap<>();
	private static Map<Integer, Location> step_location = new HashMap<>();
	private static ChatColor basicColor = ChatColor.WHITE;
	
	public static void init() {
		World w = Bukkit.getWorlds().get(0);
		msg_title.put(0, basicColor+"Parlez � "+ChatColor.DARK_PURPLE+"BOB"+basicColor+" et prendre la qu�te");
		step_location.put(1, new Location(w, -6841.5, 63, 1875.5, -62, 0));
		
		msg_title.put(1, basicColor+"Parlez � "+ChatColor.DARK_PURPLE+"Pilote"+basicColor+" et utiliser la mongolfi�re");
		step_location.put(2, new Location(w, -6841.5, 63, 1875.5, -62, 0));
		
		msg_title.put(2, basicColor+"Rendez la qu�te � "+ChatColor.DARK_PURPLE+"BOB");
		step_location.put(3, new Location(w, -6785.5, 65, 1885.5, 180, 0));
		
		msg_title.put(3, basicColor+"Parlez � "+ChatColor.DARK_PURPLE+"BOB"+basicColor+" et acceptez la qu�te");
		step_location.put(4, new Location(w, -6785.5, 65, 1885.5, 180, 0));
		
		msg_title.put(4, basicColor+"Achetez une parcelle via "+ChatColor.GREEN+"/position");
		step_location.put(5, new Location(w, -6790.5, 63, 1857.5, 120, 0));
		
		msg_title.put(4, basicColor+"Revendez la parcelle via "+ChatColor.GREEN+"/parcelle");
		step_location.put(6, new Location(w, -6790.5, 63, 1857.5, 120, 0));
		
		msg_title.put(5, basicColor+"Rendez la qu�te � "+ChatColor.DARK_PURPLE+"BOB");
		step_location.put(7, new Location(w, -6790.5, 63, 1857.5, 120, 0));
		
		msg_title.put(6, basicColor+"Parlez � "+ChatColor.DARK_PURPLE+"Pilote"+basicColor+" et utiliser la mongolfi�re");
		step_location.put(8, new Location(w, -6811.5, 63, 1849.5, 90, 0));
	}
	
	public NewTutoriel(Player player) {
		this.player = player;
		DataAPI.getPlayer(player).setShowingTutorial(true);
		nextStep();
	}
	
	public void nextStep() {
		if(index < msg_title.size()) {
			new Title("", msg_title.get(index), 5, 20*3600, 5).send(player);
			index++;
		} else {
			end();
		}
	}
	
	public int getStep() { return index; }

	private void end() {
		player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		DataAPI.getPlayer(player).setShowingTutorial(false);
		new Title(ChatColor.GOLD + "Tutoriel termin� !", ChatColor.WHITE+"Inscrivez vous � la mairie via "+ChatColor.GREEN+"/lier", 5, 20*5, 5).send(player);
	}

	public void teleportStep() {
		player.teleport(step_location.get(index));
	}
}
