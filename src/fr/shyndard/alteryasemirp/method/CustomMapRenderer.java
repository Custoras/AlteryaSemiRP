package fr.shyndard.alteryasemirp.method;

import java.net.URL;

import javax.imageio.ImageIO;

import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import fr.shyndard.alteryasemirp.Main;
import fr.shyndard.alteryasemirp.function.SQLFunction;

@SuppressWarnings("deprecation")
public class CustomMapRenderer extends MapRenderer {

	private Boolean hasRender = false;
	private Boolean create;
	private String url;
	
	public CustomMapRenderer(String url, boolean create) {
		System.out.println("url>"+url);
		this.url = url;
		this.create = create;
	}
	
	@Override
	public void render(MapView mv, MapCanvas mc, Player p) {
		if(hasRender) return;
		hasRender = true;
		if(url == null) url = Main.getMapUrl().get(p);
		if(url == null) return;
		if(create) SQLFunction.saveMapView(mv.getId(), url);
		try {
			mc.drawImage(0, 0, ImageIO.read(new URL(url)));
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		Main.addUrl(p, null);
		Main.enable = false;
	}
}
