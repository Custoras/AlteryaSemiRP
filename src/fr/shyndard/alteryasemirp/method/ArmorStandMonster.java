package fr.shyndard.alteryasemirp.method;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.EntityTypes;
import net.minecraft.server.v1_12_R1.Vector3f;
import net.minecraft.server.v1_12_R1.World;

@SuppressWarnings("unchecked")
public class ArmorStandMonster extends EntityArmorStand {

    public UUID player_uuid;

    public ArmorStandMonster(World world, Player p) {
        super(world);
        player_uuid = p.getUniqueId();
    }

    @Override
    public void B_() {
        EntityPlayer p = ((CraftPlayer) Bukkit.getPlayer(player_uuid)).getHandle();
        Player player = Bukkit.getPlayer(player_uuid);
        double radians = Math.toRadians(player.getLocation().getYaw()+180);
	    Vector vec = (player.getLocation().add(0.8D * Math.cos(radians), 0, 0.8D * Math.sin(radians)).toVector());
	    
        setPosition(vec.getX(), p.locY+1.0, vec.getZ());
        this.yaw = p.yaw;
        this.pitch = p.pitch;
        setHeadPose(new Vector3f(p.pitch, 0.0F, 0.0F));
    }

    public static ArmorStandMonster SPAWN(Player p, Integer type) {
        World w = ((CraftWorld) p.getWorld()).getHandle();
        ArmorStandMonster armorStand = new ArmorStandMonster(w, p);
        
        double radians = Math.toRadians(p.getLocation().getYaw()+180);
	    Vector vec = (p.getLocation().add(0.8D * Math.cos(radians), 0, 0.8D * Math.sin(radians)).toVector());
	    
        armorStand.setLocation(vec.getX(), vec.getY()+1.0, vec.getZ(), p.getLocation().getYaw(), p.getLocation().getPitch());
        ArmorStand as = (ArmorStand)armorStand.getBukkitEntity();
        setArmorStand(as, type);
        w.addEntity(armorStand, SpawnReason.CUSTOM);
        
        return armorStand;
    }

	public static void registerEntity() {
        try {
            Class<EntityTypes> entityTypeClass = EntityTypes.class;
            Field c = entityTypeClass.getDeclaredField("c");
            c.setAccessible(true);
            
			@SuppressWarnings("rawtypes")
			HashMap<String, Class<?>> c_map = (HashMap) c.get(null);
            c_map.put("customArmorStand", ArmorStandMonster.class);

            Field d = entityTypeClass.getDeclaredField("d");
            d.setAccessible(true);
            HashMap<Class<?>, String> d_map = (HashMap) d.get(null);
            d_map.put(ArmorStandMonster.class, "customArmorStand");

            Field e = entityTypeClass.getDeclaredField("e");
            e.setAccessible(true);
            HashMap<Integer, Class<?>> e_map = (HashMap) e.get(null);
            e_map.put(Integer.valueOf(63), ArmorStandMonster.class);

            Field f = entityTypeClass.getDeclaredField("f");
            f.setAccessible(true);
            HashMap<Class<?>, Integer> f_map = (HashMap) f.get(null);
            f_map.put(ArmorStandMonster.class, Integer.valueOf(63));

            Field g = entityTypeClass.getDeclaredField("g");
            g.setAccessible(true);
            HashMap<String, Integer> g_map = (HashMap) g.get(null);
            g_map.put("customArmorStand", Integer.valueOf(63));

        } catch (Exception exc) {
            Field d;
            int d_map;
            Method[] e;
            Class[] paramTypes = { Class.class, String.class, Integer.TYPE };
            try {
                Method method = EntityTypes.class.getDeclaredMethod(
                        "addMapping", paramTypes);
                method.setAccessible(true);
            } catch (Exception ex) {
                exc.addSuppressed(ex);
                try {
                    d_map = (e = EntityTypes.class.getDeclaredMethods()).length;
                    for (int d1 = 0; d1 < d_map; d1++) {
                        Method method = e[d1];
                        if (Arrays.equals(paramTypes,
                                method.getParameterTypes())) {
                            method.invoke(null, new Object[] {
                                    ArmorStandMonster.class,
                                    "customArmorStand", Integer.valueOf(63) });
                        }
                    }
                } catch (Exception exe) {
                    exc.addSuppressed(exe);
                }
                exc.printStackTrace();
            }
        }
    }
	
	private static void setArmorStand(ArmorStand as, Integer type) {
		as.setBasePlate(false);
		as.setSmall(true);
		as.setInvulnerable(true);
		as.setGravity(false);
		as.setArms(true);
		as.setVisible(true);
		as.setCollidable(false);
		as.setCanPickupItems(false);
		as.setNoDamageTicks(Integer.MAX_VALUE);
		setArmor(as, type);
	}
	
	public static void setArmor(ArmorStand as, Integer type) {
		if(type == 0) {			
			setEquipment(as, Color.WHITE, new ItemStack(Material.SKULL_ITEM, 1, (short)0));
		}
		else if(type == 1) {
			setEquipment(as, Color.BLACK, new ItemStack(Material.SKULL_ITEM, 1, (short)1));
		}
		else if(type == 2) {
			setEquipment(as, Color.GREEN, new ItemStack(Material.SKULL_ITEM, 1, (short)2));
		}
		else if(type == 3) {
			setEquipment(as, Color.MAROON, new ItemStack(Material.SKULL_ITEM, 1, (short)3));
		}
		else if(type == 4) {
			setEquipment(as, Color.GREEN, new ItemStack(Material.SKULL_ITEM, 1, (short)4));
		}
		else if(type == 5) {
			setEquipment(as, Color.BLACK, new ItemStack(Material.SKULL_ITEM, 1, (short)5));
		}
		else {
			setEquipment(as, Color.BLACK, new ItemStack(Material.DIAMOND_BLOCK));
		}
	}
	
	static void setEquipment(ArmorStand as, Color color, ItemStack item) {
		ItemStack chestplate = new ItemStack(Material.matchMaterial("299"), 1);
		LeatherArmorMeta PlastronMeta = (LeatherArmorMeta) chestplate.getItemMeta();
			PlastronMeta.setColor(color);
			chestplate.setItemMeta(PlastronMeta);
		ItemStack leggins = new ItemStack(Material.matchMaterial("300"), 1);
		LeatherArmorMeta JambieresMeta = (LeatherArmorMeta) leggins.getItemMeta();
			JambieresMeta.setColor(color);
			leggins.setItemMeta(JambieresMeta);
		ItemStack boots = new ItemStack(Material.matchMaterial("301"), 1);
		LeatherArmorMeta BottesMeta = (LeatherArmorMeta) boots.getItemMeta();
			BottesMeta.setColor(color);
			boots.setItemMeta(BottesMeta);
		as.setHelmet(item);
		as.setChestplate(chestplate);
		as.setLeggings(leggins);
		as.setBoots(boots);
	}
}