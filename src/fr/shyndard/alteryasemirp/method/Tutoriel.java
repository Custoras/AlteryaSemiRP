package fr.shyndard.alteryasemirp.method;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryasemirp.Main;
import net.md_5.bungee.api.ChatColor;

public class Tutoriel {

	private Player player;
	private Integer index = 0;
	
	private static String prefix = "[" + ChatColor.GREEN + ChatColor.BOLD + "?" + ChatColor.WHITE + "] " + ChatColor.GRAY;
	private static List<String> msg = new ArrayList<>();
	private static List<Location> location = new ArrayList<>();
	
	public static void init() {
		World world = Bukkit.getWorlds().get(0);
		location.add(new Location(world, -95, 81, 227, -63, 4.5F));
		msg.add("Vous vivez actuellement dans le pays d'Alterya. Nom du roi fondateur il y a 200 ans.");
		
		location.add(new Location(world, -86.5, 65.5, 218, 0, 8));
		msg.add("Vous �tes dans la capitale : Alterya. Centre de toutes les activit�s du pays.");
		
		location.add(new Location(world, -72.5, 68.5, 276.5, 90, 2));
		msg.add("Dans chaque ville se trouve un " + ChatColor.DARK_PURPLE + "banquier" + ChatColor.GRAY + ". Votre bourse est limit�e mais pas votre compte en banque. Si vous mourrez, vous perdez le contenu de votre bourse.");
		
		location.add(new Location(world, -79.5, 66, 239.5, -40, 15));
		msg.add("La capitale comporte un march� de mati�res premi�res. Les valeurs sont �lev�es mais rien ne vous emp�che de cr�er votre propre point de vente.");
		
		location.add(new Location(world, -95.5, 69, 250.5, 23, -15));
		msg.add("Les mongolfi�res vous permettent de parcourir rapidement le pays. Un " + ChatColor.DARK_PURPLE + "pilote" + ChatColor.GRAY + " est rep�rable via son icone d'avion au desus de la t�te.");
		
		location.add(new Location(world, -113, 70, 205.5, -146, 25));
		msg.add("Vous pouvez acheter des parcelles dans les villes. Rendez vous � " + ChatColor.GOLD + "Tameryl" + ChatColor.GRAY + " pour en obtenir une gratuitement.");
		
		location.add(new Location(world, -58.5, 111, 209, -104, 11));
		msg.add("Sortez de la ville et louez des chunks. Vous en avez 4 au maximum. Montez en grade pour en d�bloquer plus.");
		
		location.add(new Location(world, -86.5, 65.5, 218, 0, 8));
		msg.add("Utilisez " + ChatColor.GREEN + "/position" + ChatColor.GRAY + " pour obtenir les informations sur votre emplacement et les possibilit�s d'achat ou de location.");
		
		location.add(new Location(world, -79.5, 65.5, 227.5, -53, 23));
		msg.add("Les " + ChatColor.DARK_PURPLE + "habitants" + ChatColor.GRAY + " de ce pays vous propose r�guli�rement des missions en �change d'une r�compense. Venez les voir quotidiennement.");
	}
	
	public Tutoriel(Player player) {
		this.player = player;
		start();
	}

	private void start() {
		DataAPI.getPlayer(player).setShowingTutorial(true);
		player.setGameMode(GameMode.SPECTATOR);
		player.setFlySpeed(0);
		player.setAllowFlight(true);
		player.setFlying(true);
		launch();
	}
	
	private void launch() {
		player.teleport(location.get(index));
		player.sendMessage(prefix + msg.get(index));
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  index++;
	        	  if(player.isOnline()) {
		        	  if(location.size() > index) launch();
		        	  else {
		        		  resetPlayer(player);
		        		  PlayerInformation pi = DataAPI.getPlayer(player);
		        		  pi.setShowingTutorial(false);
		        		  player.sendMessage(prefix + "Vous venez de compl�ter le tutoriel. Pour + d'infos : " + ChatColor.GREEN + "/help");
		  				  player.sendMessage(ChatColor.GRAY + "Inscrivez vous via " + ChatColor.LIGHT_PURPLE + "/lier" + ChatColor.GRAY + " pour devenir Citoyen.");
		        	  }
	        	  }
	          }
		}, 9*20L);		
	}
	
	public static void resetPlayer(Player player) {
		player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		player.setGameMode(GameMode.SURVIVAL);
		player.setFlySpeed(1);
		player.setAllowFlight(false);
		player.setFlying(false);
	}
}
