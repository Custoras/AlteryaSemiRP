package fr.shyndard.alteryasemirp.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.MapInitializeEvent;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import fr.shyndard.alteryasemirp.Main;
import fr.shyndard.alteryasemirp.method.CustomMapRenderer;

public class MapRender implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMap(MapInitializeEvent event) {
		System.out.println(event.getMap().getId());
		if(!Main.enable) return;
		MapView v = event.getMap();
		for(MapRenderer mr : v.getRenderers()) v.removeRenderer(mr);
		v.addRenderer(new CustomMapRenderer(null, true));
	}
}
