package fr.shyndard.alteryasemirp.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaquest.api.QuestAPI;
import fr.shyndard.alteryasemirp.function.BallooningManagement;
import net.citizensnpcs.api.event.NPCLeftClickEvent;

public class PlayerInteractNPC implements Listener {
	
	@EventHandler
	public void onInteract(NPCLeftClickEvent event) {
		if(event.isCancelled()) return;
		PlayerInformation pi = DataAPI.getPlayer(event.getClicker());
		if(BallooningManagement.existPilot(event.getNPC().getId())) {
			if(pi.hasPermission("ballooning.access")) {
				NPCAPI.talkTo(event.getNPC(), event.getClicker(), "Hey ! Un ptit tour en ballon, �a te branche ?");
				NPCAPI.answer(event.getClicker(), "Pourquoi pas, en route vers les �toiles !", "/ballooning openmenu " + event.getNPC().getId());
				NPCAPI.answer(event.getClicker(), "Je pr�f�re marcher", "/ballooning deny " + event.getNPC().getId());
			} else QuestAPI.sendRandomMessage(event.getNPC(), event.getClicker());
			event.setCancelled(true);
		}
	}
}