package fr.shyndard.alteryasemirp.event;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.shyndard.alteryasemirp.function.BallooningManagement;

public class PlayerInteractMenu implements Listener {
	
	@EventHandler
	public void onInteract(InventoryClickEvent event) {
		if(event.getClickedInventory() == null) return;
		if(event.getView().getTopInventory() != null 
				&& event.getView().getTopInventory().getName().equals(BallooningManagement.getInvName())
				&& !event.getClickedInventory().getName().equals(BallooningManagement.getInvName())
				&& event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)
		{
			event.setCancelled(true);
			return;
		}
		if(!event.getClickedInventory().getName().equals(BallooningManagement.getInvName())) return;
		event.setCancelled(true);
		BallooningManagement.teleport((Player)event.getWhoClicked(), event.getSlot());
	}
}
