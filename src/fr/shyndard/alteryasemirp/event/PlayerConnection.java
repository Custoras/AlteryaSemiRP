package fr.shyndard.alteryasemirp.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.function.NameColorManagement;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaapi.method.Title;
import fr.shyndard.alteryasemirp.function.CustomEntityManager;
import fr.shyndard.alteryasemirp.function.MapManager;
import fr.shyndard.alteryasemirp.method.Tutoriel;
import net.md_5.bungee.api.ChatColor;

public class PlayerConnection implements Listener {
	
	@EventHandler
	public void onJoinEvent(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if(!MapManager.init) MapManager.init();
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(event.getPlayer().hasPlayedBefore()) {
			new Title(ChatColor.GOLD + "Alterya", ChatColor.DARK_GREEN + "Bon retour parmi nous", 10, 100, 10).send(player);
			if(pi.getRank().getPower() == 10) {
				player.sendMessage(ChatColor.GRAY + "Vous �tes actuellement " + pi.getRank().getColor() + pi.getRank().getName() + ChatColor.GRAY + ".");
				player.sendMessage(ChatColor.GRAY + "Inscrivez vous via " + ChatColor.LIGHT_PURPLE + "/lier" + ChatColor.GRAY + " pour devenir Citoyen.");
			}
		} else {
			new Title(ChatColor.GOLD + "Bienvenue", ChatColor.DARK_GREEN + "dans le monde d'Alterya !", 10, 200, 10).send(player);
			Bukkit.broadcastMessage(ChatColor.GREEN + "Bienvenue � " + ChatColor.AQUA + event.getPlayer().getName() + ChatColor.GREEN + " dans le monde d'Alterya !");
		}
		NameColorManagement.update(player);
	}
	
	@EventHandler
	public void onQuitEvent(PlayerQuitEvent event) {
		if(DataAPI.getPlayer(event.getPlayer()).isShowingTutorial()) {
			Tutoriel.resetPlayer(event.getPlayer());
		}
		CustomEntityManager.despawn(event.getPlayer());
		NameColorManagement.remove(event.getPlayer());
	}
}
