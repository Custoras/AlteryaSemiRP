package fr.shyndard.alteryasemirp.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaareamanagement.AreaManagementAPI;
import fr.shyndard.alteryaareamanagement.method.AreaParam;
import net.md_5.bungee.api.ChatColor;

public class PlayerDeath implements Listener {

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		AreaParam param = AreaManagementAPI.getActualLocationParam(event.getEntity().getLocation());
		if(param == null || param.isDeathLoseMoneyEnabled()) {
			PlayerInformation pi = DataAPI.getPlayer(event.getEntity());
			int newBalance = (int)(pi.getMoney(false)/2);
			event.getEntity().sendMessage(ChatColor.RED + "Vous l�chez " + newBalance + DataAPI.getMoneySymbol());
			pi.setMoney((float)newBalance, false, false);
		}
	}
}
