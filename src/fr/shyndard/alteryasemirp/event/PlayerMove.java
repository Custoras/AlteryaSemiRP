package fr.shyndard.alteryasemirp.event;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryasemirp.function.TutorialManager;

public class PlayerMove implements Listener {

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		if(event.getTo().getBlock().getType() == Material.WATER || event.getTo().getBlock().getType() == Material.STATIONARY_WATER) {
			if(event.getFrom().getBlock() != event.getTo().getBlock()) {
				if(DataAPI.getPlayer(event.getPlayer()).isShowingTutorial()) {
					TutorialManager.get(event.getPlayer()).teleportStep();
					event.getPlayer().sendMessage("Outch, l'eau est glac�e ici...");
				}
			}
		}
	}
}
