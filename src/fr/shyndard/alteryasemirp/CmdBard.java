package fr.shyndard.alteryasemirp;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryasemirp.function.BardManager;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class CmdBard implements CommandExecutor {

	String prefix = ChatColor.GRAY + "[Barde] ";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(prefix + "La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("bard.edit")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if(args.length == 0) {
			sendHelp(sender);
			return false;
		}
		if(args[0].equalsIgnoreCase("reload")) {
			BardManager.init();
			sender.sendMessage(prefix + ChatColor.GREEN + "Reload effectu�.");
		}
		if(args[0].equalsIgnoreCase("speechadd")) {
			if(args.length >= 3) {
				try {
					NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.parseInt(args[1]));
					if(npc == null) {
						sender.sendMessage(prefix + ChatColor.RED + "Identifiant du PNJ inconnu.");
						return false;
					}
					String speech = args[2];
					for(int i = 3; i < args.length; i++) speech = speech + " " + args[i];
					if(BardManager.addSpeech(npc.getId(), speech)) {
						sender.sendMessage(prefix + ChatColor.GREEN + "Phrase ajout�e !");
					} else {
						sender.sendMessage(prefix + ChatColor.RED + "Impossible d'ajouter cette phrase !");
					}
				} catch(Exception ex) {
					sender.sendMessage(prefix + ChatColor.RED + "Erreur : /bard speechadd <npc_id> <phrase>");
				}
			} else {
				sender.sendMessage(prefix + ChatColor.RED + "/bard speechadd <id> <phrase>");
			}
			return false;
		}
		if(args[0].equalsIgnoreCase("clearspeech")) {
			if(args.length == 2) {
				try {
					NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.parseInt(args[1]));
					if(npc == null) {
						sender.sendMessage(prefix + ChatColor.RED + "Identifiant du PNJ inconnu.");
						return false;
					}
					if(BardManager.clearSpeech(npc.getId())) {
						sender.sendMessage(prefix + ChatColor.GREEN + "Messages supprim�s !");
					} else {
						sender.sendMessage(prefix + ChatColor.RED + "Impossible d'ajouter cette phrase !");
					}
				} catch(Exception ex) {
					sender.sendMessage(prefix + ChatColor.RED + "Erreur : /bard clearspeech <npc_id>");
				}
			} else {
				sender.sendMessage(prefix + ChatColor.RED + "Erreur : /bard clearspeech <id>");
			}
			return false;
		}
		if(args[0].equalsIgnoreCase("playAll")) {
			BardManager.playAll();
			sender.sendMessage(prefix + ChatColor.GREEN + "Tous les bardes chantent !");
			return false;
		}
		if(args[0].equalsIgnoreCase("play")) {
			if(args.length == 2) {
				try {
					NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.parseInt(args[1]));
					if(npc == null) {
						sender.sendMessage(prefix + ChatColor.RED + "Identifiant du PNJ inconnu.");
						return false;
					}
					BardManager.play(npc.getId());
					sender.sendMessage(prefix + npc.getFullName() + ChatColor.GREEN + " se met � chanter !");
				} catch(Exception ex) {
					sender.sendMessage(prefix + ChatColor.RED + "Erreur : /bard play <npc_id>");
				}
			}
			return false;
		}
		sendHelp(sender);
		return false;
	}
	
	void sendHelp(CommandSender sender) {
		sender.sendMessage(ChatColor.GRAY + "Commandes barde");
		sender.sendMessage(ChatColor.GRAY + "/bard playall");
		sender.sendMessage(ChatColor.GRAY + "/bard play <npc_id>");
		sender.sendMessage(ChatColor.GRAY + "/bard clearspeech <npc_id>");
		sender.sendMessage(ChatColor.GRAY + "/bard speechadd <npc_id> <une_phrase>");
	}

}
