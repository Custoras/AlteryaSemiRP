package fr.shyndard.alteryasemirp;

import java.net.URL;

import javax.imageio.ImageIO;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryasemirp.function.MapManager;
import net.md_5.bungee.api.ChatColor;

public class CmdMap implements CommandExecutor {

	String prefix = ChatColor.GRAY + "[Mongolgière] ";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(prefix + "La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("ballooning.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if(args.length > 0) {
			if(args[0].equalsIgnoreCase("url") && args.length == 2) {
				try {
					ImageIO.read(new URL(args[1]));
				} catch(Exception ex) {
					sender.sendMessage(ChatColor.RED + "Impossible de charger cet url");
					return false;
				}
				Main.addUrl(player, args[1]);
				sender.sendMessage("Url set");
				return false;
			}
			if(args[0].equalsIgnoreCase("enable")) {
				if(Main.enable) Main.enable = false;
				else Main.enable = true;
				sender.sendMessage("Url render set on " + Main.enable);
				return false;
			}
			if(args[0].equalsIgnoreCase("init")) {
				MapManager.init();
				sender.sendMessage("Initialisé");
				return false;
			}
			sendHelp(sender);
		} else sendHelp(sender);
		return false;
	}
	
	void sendHelp(CommandSender sender) {
		sender.sendMessage(ChatColor.GRAY + "Commandes génération map");
	}

}
