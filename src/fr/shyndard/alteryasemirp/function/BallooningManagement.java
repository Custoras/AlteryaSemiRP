package fr.shyndard.alteryasemirp.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryasemirp.Main;
import fr.shyndard.alteryasemirp.method.Ballooning;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class BallooningManagement {

	static List<Ballooning> ballooning_list = new ArrayList<>();
	static Inventory inv;
	
	public static void initMenu() {
		loadPilot();
		int max = 0;
		for(Ballooning b : ballooning_list) {
			if(b.getMenuPosition()>max) max = b.getMenuPosition();
		}
		inv = Bukkit.createInventory(null, (Math.round(max/9)+1)*9, getInvName());
		for(Ballooning b : ballooning_list) {
			ItemStack is = new ItemStack(Material.MAP);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(ChatColor.RED + "Direction > " + ChatColor.GOLD + b.getArrivalName());
			is.setItemMeta(im);
			inv.setItem(b.getMenuPosition(), is);
		}
	}
	
	public static void openMenu(Player player) {
		player.openInventory(inv);
	}
	
	public static List<Ballooning> getBalooningList() {
		return ballooning_list;
	}
	
	public static void loadPilot() {
		removeAll();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT pilot_id, arrival_name, loc_world, loc_x, loc_y, loc_z, loc_yaw, loc_pitch, menu_position FROM pilot_list");
			while(result.next()) {
				ballooning_list.add(new Ballooning(
						result.getInt("pilot_id"),
						result.getInt("menu_position"), 
						new Location(Bukkit.getWorld(
								result.getString("loc_world")), 
								result.getFloat("loc_x"),
								result.getFloat("loc_y"),
								result.getFloat("loc_z"),
								result.getInt("loc_yaw"),
								result.getInt("loc_pitch")
								),
						result.getString("arrival_name")));
				Main.getPlugin().getLogger().info("Load ballooning " + result.getInt("pilot_id") + " done.");
			}
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static boolean existPilot(Integer pilot_id) {
		for(Ballooning b : ballooning_list) if(b.getPilotId() == pilot_id) return true;
		return false;
	}
	
	public static void teleport(Player player, Integer menu_slot) {
		for(Ballooning b : ballooning_list) {
			if(b.getMenuPosition() == menu_slot) {
				NPC npc = CitizensAPI.getNPCRegistry().getById(b.getPilotId());
				if(npc == null) {
					player.sendMessage(ChatColor.RED + "La destination " + ChatColor.GOLD + b.getArrivalName() + ChatColor.RED + " est indisponible.");
				} else {
					NPCAPI.talkTo(npc, player, "En route vers " + ChatColor.GOLD + b.getArrivalName());
					player.teleport(b.getArrivalLocation());
				}
				player.closeInventory();
				player.updateInventory();
				return;
			}
		}
	}

	public static void set(Integer npc_id, String name, Location location, Integer slot) {
		for(Ballooning b : ballooning_list) {
			if(b.getArrivalName().equalsIgnoreCase(name)) {
				SQLFunction.updateBallooning(npc_id, name, location, slot);
				initMenu();
				return;
			}
		}
		SQLFunction.createBallooning(npc_id, name, location, slot);
		initMenu();
	}

	public static boolean deleteBallooning(String name) {
		for(Ballooning b : ballooning_list) {
			if(b.getArrivalName().equalsIgnoreCase(name)) {
				b.remove();
				SQLFunction.deleteBallooning(name);
				CitizensAPI.getNPCRegistry().getById(b.getPilotId()).destroy();
				initMenu();
				return true;
			}
		}
		return false;
	}

	public static String getInvName() { return "Mongolfière"; }
	
	public static void removeAll() {
		for(Ballooning b : ballooning_list) b.remove();
		ballooning_list.clear();
	}
}
