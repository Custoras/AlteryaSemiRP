package fr.shyndard.alteryasemirp.function;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import fr.shyndard.alteryasemirp.method.ArmorStandMonster;
import fr.shyndard.alteryasemirp.method.ArmorStandTitle;

public class CustomEntityManager {

	private static Map<Player, ArmorStandMonster> player_monster_list = new HashMap<>();
	private static Map<Player, ArmorStandTitle> player_title_list = new HashMap<>();
	
	public static void spawnMonster(Player player, Integer value) {
		if(player_monster_list.get(player) != null) {
			player_monster_list.get(player).die();
		}
		player_monster_list.put(player, ArmorStandMonster.SPAWN(player, value));
	}
	
	public static void spawnTitle(Player player, String name) {
		if(player_title_list.get(player) != null) {
			player_title_list.get(player).die();
		}
		player_title_list.put(player, ArmorStandTitle.SPAWN(player, name));
	}
	
	public static void despawn(Player player) {
		if(player_monster_list.get(player) != null) {
			player_monster_list.get(player).die();
		}
		if(player_title_list.get(player) != null) {
			player_title_list.get(player).die();
		}
	}
}
