package fr.shyndard.alteryasemirp.function;

import java.sql.PreparedStatement;
import java.sql.Types;
import org.bukkit.Location;
import fr.shyndard.alteryaapi.api.DataAPI;

public class SQLFunction {

	public static void updateBallooning(Integer npc_id, String name, Location loc, Integer slot) {
		try {
			String sql = "UPDATE pilot_list SET "
					+ "pilot_id = ?,"
					+ "loc_world = ?,"
					+ "loc_x = ?,"
					+ "loc_y = ?,"
					+ "loc_z = ?,"
					+ "loc_yaw = ?,"
					+ "loc_pitch = ?,"
					+ "menu_position = ? "
					+ "WHERE arrival_name = ?;"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, npc_id, Types.INTEGER);
			statement.setObject(2, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(3, loc.getX(), Types.FLOAT);
			statement.setObject(4, loc.getY(), Types.FLOAT);
			statement.setObject(5, loc.getZ(), Types.FLOAT);
			statement.setObject(6, loc.getYaw(), Types.INTEGER);
			statement.setObject(7, loc.getPitch(), Types.INTEGER);
			statement.setObject(8, slot, Types.INTEGER);
			statement.setObject(9, name, Types.VARCHAR);
			statement.executeUpdate(); 
		} catch(Exception ex) { ex.printStackTrace(); }
	}

	public static void createBallooning(Integer npc_id, String name, Location loc, Integer slot) {
		try {
			String sql = "INSERT INTO pilot_list VALUES (?,?,?,?,?,?,?,?,?);"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, npc_id, Types.INTEGER);
			statement.setObject(2, name, Types.VARCHAR);
			statement.setObject(3, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(4, loc.getX(), Types.FLOAT);
			statement.setObject(5, loc.getY(), Types.FLOAT);
			statement.setObject(6, loc.getZ(), Types.FLOAT);
			statement.setObject(7, loc.getYaw(), Types.INTEGER);
			statement.setObject(8, loc.getPitch(), Types.INTEGER);
			statement.setObject(9, slot, Types.INTEGER);
			statement.executeUpdate(); 
		} catch(Exception ex) { ex.printStackTrace(); }
	}
	
	public static void saveMapView(Short id, String url) {
		try {
			String sql = "INSERT INTO mapview VALUES (?, ?);"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, id, Types.INTEGER);
			statement.setObject(2, url, Types.VARCHAR);
			statement.executeUpdate(); 
		} catch(Exception ex) { ex.printStackTrace(); }
	}
	
	public static void deleteBallooning(String name) {
		try {
			String sql = "DELETE FROM pilot_list WHERE arrival_name = ?"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, name, Types.VARCHAR);
			statement.executeUpdate(); 
		} catch(Exception ex) { ex.printStackTrace(); }
	}	
}
