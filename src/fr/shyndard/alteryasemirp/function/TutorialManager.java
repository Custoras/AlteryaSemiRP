package fr.shyndard.alteryasemirp.function;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import fr.shyndard.alteryasemirp.method.NewTutoriel;

public class TutorialManager {

	private static Map<Player, NewTutoriel> player_tuto_list = new HashMap<>();
	
	public static boolean exist(Player player) {
		return player_tuto_list.get(player) != null;
	}
	
	public static NewTutoriel get(Player player) {
		return player_tuto_list.get(player);
	}
	
	public static void add(Player player) {
		player_tuto_list.put(player, new NewTutoriel(player));
	}

}
