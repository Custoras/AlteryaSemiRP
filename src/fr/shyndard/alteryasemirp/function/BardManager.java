package fr.shyndard.alteryasemirp.function;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryasemirp.Main;
import fr.shyndard.alteryasemirp.method.Bard;

public class BardManager {

	static List<Bard> bard_list = new ArrayList<>();
	
	public static void init() {
		for(Bard b : bard_list) b.remove();
		bard_list.clear();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT DISTINCT bard_id FROM bard_speech");
			while(result.next()) bard_list.add(new Bard(result.getInt("bard_id")));
		} catch (SQLException e) {e.printStackTrace();}
		new BukkitRunnable() {
			@Override
            public void run() { playAll(); }
		}.runTaskTimer(Main.getPlugin(), 10*20L, 10*60*20L);
	}
	
	public static void playAll() {
		for(Bard b : bard_list) b.play();
	}

	public static void play(Integer id) {
		for(Bard b : bard_list) {
			if(b.getBardId() == id) b.play();
			return;
		}
	}

	public static boolean addSpeech(int id, String text) {
		try {
			Integer value = 0;
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT MAX(speech_index) AS bard_id FROM bard_speech WHERE bard_id = " + id);
			while(result.next()) { value = result.getInt("bard_id")+1; }
			String sql = "INSERT INTO bard_speech VALUES (" + id + ", " + value + ", ?)"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql); 
			statement.setObject(1, text, Types.VARCHAR); 
			statement.executeUpdate();
		} catch(Exception ex) { ex.printStackTrace(); return false; }
		init();
		return true;
	}

	public static boolean clearSpeech(int id) {
		try {
			String sql = "DELETE FROM bard_speech WHERE bard_id = ?" ; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql); 
			statement.setObject(1, id, Types.INTEGER); 
			statement.executeUpdate();
		} catch(Exception ex) { ex.printStackTrace(); return false; }
		init();
		return true;
	}
}
