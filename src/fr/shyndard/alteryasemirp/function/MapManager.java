package fr.shyndard.alteryasemirp.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryasemirp.method.CustomMapRenderer;

@SuppressWarnings("deprecation")
public class MapManager {

	public static boolean init = false;
	
	public static void init() {
		Map<Short, String> data = getMapUrl();
		for(Short map_id : getMapUrl().keySet()) {
			MapView map = Bukkit.getMap(map_id);
			for(MapRenderer mr : map.getRenderers()) map.removeRenderer(mr);
			map.addRenderer(new CustomMapRenderer(data.get(map_id), false));
		}
		init = true;
	}
	
	private static Map<Short, String> getMapUrl() {
		Map<Short, String> map_url = new HashMap<>();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT * FROM mapview");
			while(result.next()) {
				map_url.put(result.getShort("map_id"), result.getString("map_url"));
			}
		} catch (SQLException e) {e.printStackTrace();}
		return map_url;
	}
}
